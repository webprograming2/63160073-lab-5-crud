import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeout = ref(1000);
  const showMessage = (msg: string, tout: number = 1000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = tout;
  };
  const clearMessage = () => {
    message.value = "";
    isShow.value = false;
  };
  return { isShow, message, showMessage, closeMessage: clearMessage, timeout };
});
