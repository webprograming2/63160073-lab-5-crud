import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";
export const useLoginStore = defineStore("login", () => {
  const userStore = useUserStore();
  const messageStore = useMessageStore();
  const username = ref("");
  const isLogin = computed(() => {
    return username.value !== "";
  });
  const login = (user: string, password: string): void => {
    if (userStore.login(user, password)) {
      username.value = user;
      localStorage.setItem("username", user);
    } else {
      messageStore.showMessage("username or password incorrect");
    }
  };
  const logout = (): void => {
    username.value = "";
    localStorage.removeItem("username");
  };
  const loadData = () => {
    username.value = localStorage.getItem("username") || "";
  };
  return { username, isLogin, login, logout, loadData };
});
